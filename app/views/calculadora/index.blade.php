@extends('template.layout')

@section('content')
   @include('clientes.relatorios')

      <div class="alert alert-success" role="alert">
         <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
      </div>
      <div class="row">
      <br/>
      <div class="col-md-12">
      <form method="get" id="conversor"  action="{{action('CalculadoraController@binomial')}}">
         <div class="form-group">
            <label for="exampleInputConversor">Calcular distribuição:</label>
         </div>
         <button type="submit" class="btn btn-success">
         Binomial
         </button>
      </form></br>
      <form method="get" id="conversor" action="{{action('CalculadoraController@geometrica')}}">
         <button type="submit" class="btn btn-success">
         Geométrica
         </button>
      </form></br>
      <form method="get" id="conversor" action="{{action('CalculadoraController@binomialnegativa')}}">
         <button type="submit" class="btn btn-success">
         Binomial negativa
         </button>
      </form></br>
      <form method="get" id="conversor" action="{{action('CalculadoraController@hipergeometrica')}}">
         <button type="submit" class="btn btn-success">
         Hiper geométrica
         </button>
      </form></br>
      <form method="get" id="conversor" action="{{action('CalculadoraController@poisson')}}">
         <button type="submit" class="btn btn-success">
         Poisson
         </button>
      </form>
@stop