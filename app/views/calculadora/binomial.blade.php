@extends('template.layout')

@section('content')
@include('clientes.relatorios')

@define $number = (empty(Input::get('number'))) ? 0 : Input::get('number');
@define $p      = (empty(Input::get('p'))) ? 0 : Input::get('p');
@define $x      = (empty(Input::get('x'))) ? 0 : Input::get('x');

@define $select     = (empty(Input::get('select'))) ? 0 : Input::get('select');

   <div class="alert alert-success" role="alert">
      <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
   </div>
   <div>
      <form method="get" action="{{action('CalculadoraController@binomial')}}">

      <select name="select">
         <option value="1">igual</option>
         <option value="2">menor igual </option>
         <option value="3">maior</option>
      </select>
         <div class="form-group">
            <label for="number">Número de experimentos
            </label>
            <input type="number" class="form-control" id="number" name="number" value="{{$number}}" placeholder="Digite um número">
            <label for="p">P
            </label>
            <input type="float" class="form-control" id="p" name="p" value="{{$p}}" placeholder="Digite o valor de P">
            <label for="x">X
            </label>
            <input type="float" class="form-control" id="x" name="x" value="{{$x}}" placeholder="Digite o valor de X">
         </div>
         <div class="button">
            <button type="submit" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i>
            Enviar
            </button>
         </div>
      </form>
   </div>
         <p></p>
@if( $number && $p && $x && $select)
    <?php
   function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }

   return $calc;
   }
           
            if($number = $_GET['number']) {
               $p = $_GET['p'];
               $q = 1-$p;
               $x = $_GET['x'];
               $numerooficial = $number;
               $select = $_GET['select'];
              
               $esperanca = $number*$p;
               $variancia = $esperanca*$q;

            if($select ==  '1'){
               for ($i=0; $i <= $x ; $i++) { 
                  $combinacao = fatorial($number) / (fatorial($i)* fatorial($number - $i));
                  $resultado = $combinacao*pow($p,$i)*pow($q,$number-$i);
               }
               $probabilidade = $resultado*100;
            }

      if ($select == '2') {
         $probabilidade = 0;
              for ($i=0; $i <= $x ; $i++) { 
                  $combinacao = fatorial($number) / (fatorial($i)* fatorial($number - $i));
                  $resultado = $combinacao*pow($p,$i)*pow($q,$number-$i);
               }
               $probabilidade = 1 - $resultado;


            }

               if ($select == '3') {
              for ($i=$x; $i <= $number ; $i++) { 
                  $combinacao = fatorial($number) / (fatorial($i)* fatorial($number - $i));
                  $resultado = $combinacao*pow($p,$i)*pow($q,$number-$i);
               }

               $probabilidade = 1-$resultado;

            }
            // elseif ($select == '3') {
            //   for ($i=0; $i <= $x ; $i++) { 
            //       $combinacao = fatorial($number) / (fatorial($i)* fatorial($number - $i));
            //       $probabilidade = $combinacao*pow($p,$i)*pow($q,$number-$i);
            //    }
            // }

            ?>

         <div class="row">
            <div class="alert alert-info" role="alert">
               <b>Número:</b> <?=$numerooficial?><br/>
               <b>Esperança:</b> <?=$esperanca?><br/>
               <b>Variância:</b> <?=$variancia?><br/>
               <b>Probabilidade:</b>
               <?=$probabilidade?>
               <br/>
            </div>
         </div>
      </div>
     
      <?php
         }
         ?>
@endif

@stop