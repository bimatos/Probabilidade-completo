@extends('template.layout')

@section('content')
@include('clientes.relatorios')

@define $number =  (empty(Input::get('number'))) ? 0 : Input::get('number');
@define $p =  (empty(Input::get('p'))) ? 0 : Input::get('p');
@define $r =  (empty(Input::get('r'))) ? 0 : Input::get('r');

            <div class="col-md-6">
               <div class="alert alert-success" role="alert">
                  <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
               </div>
               <div class="row">
                  <br/>
                  <div class="col-md-12">
                     <form method="get" action="{{action('CalculadoraController@binomialnegativa')}}">
                        <div class="form-group">
                           <label for="number">Número de experimentos
                           </label>
                           <input type="number" class="form-control" id="number" name="number" value="{{$number}}" placeholder="Digite um número">
                           <label for="p">P
                           </label>
                           <input type="float" class="form-control" id="p" name="p" value="{{$p}}" placeholder="Digite o valor de P">
                           <label for="r">R
                           </label>
                           <input type="float" class="form-control" id="r" name="r" value="{{$r}}" placeholder="Digite o valor de R">
                        </div>
                        <div class="button">
                           <button type="submit" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i>
                           Enviar
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
         <p></p>
         @if( $number && $p && $r)
         <?php
         function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }
   return $calc;
}

            if($number = $_GET['number']) {
               $p = $_GET['p'];
               $q = 1-$p;
               $r = $_GET['r'];
               $esperanca = $r/$p;
               $variancia = ($r*$q)/pow($p,2);
            
               for ($i=0; $i <= $number; $i++) { 
               $combinacao = fatorial($number-1) / (fatorial($r-1)*fatorial(($number-1)-($r-1)));

               $probabilidade = $combinacao*pow($p,$r)*pow($q,$number-$r);

            }
              
            
            ?>
            <div class="alert alert-info" role="alert">
               <b>Esperança:</b> <?=$esperanca?><br/>
               <b>Variância:</b> <?=$variancia?><br/>
               <b>Probabilidade:</b>
               <?=$probabilidade*100?>%
               <br/>
         </div>

      <?php } ?>
@endif
@stop