@extends('template.layout')

@section('content')
@include('clientes.relatorios')

@define $number =  (empty(Input::get('number'))) ? 0 : Input::get('number');
@define $λ =  (empty(Input::get('λ'))) ? 0 : Input::get('λ');
               <div class="alert alert-success" role="alert">
                  <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
               </div>
               <div class="row">
                  <br/>
                  <div class="col-md-12">
                     <form method="get" action="{{action('CalculadoraController@poisson')}}">
                        <div class="form-group">
                           <label for="number">Número de experimentos
                           </label>
                           <input type="number" class="form-control" id="number" name="number" value="{{$number}}" placeholder="Digite um número">
                          
                           <label for="λ">λ
                           </label>
                           <input type="float" class="form-control" id="λ" name="λ" value="{{$λ}}" placeholder="Digite o valor de λ">

                          </div>
                        <div class="button">
                           <button type="submit" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i>
                           Enviar
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
         @if( $number && $λ)

         <?php
         function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }

   return $calc;
   }
            if($number = $_GET['number']) {
               $λ = $_GET['λ'];

                         
               $esperanca = $λ;
               $variancia = $λ;  
               $probabilidade = ((pow(2.719, -$λ)) * (pow($λ,$number))) / fatorial($number);
              
            
            ?>
            <div class="alert alert-info" role="alert">
               <b>Esperança:</b> {{$esperanca}}<br/>
               <b>Variância:</b> {{$variancia}}<br/>
               <b>Probabilidade:</b>
               {{$probabilidade*100}}%
               <br/>
            </div>
         </div>
      </div>
 
      <?php
         }
         ?>
@endif
@stop