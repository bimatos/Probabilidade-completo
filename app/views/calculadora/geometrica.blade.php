@extends('template.layout')

@section('content')
@include('clientes.relatorios')

@define $number =  (empty(Input::get('number'))) ? 0 : Input::get('number');
@define $p =  (empty(Input::get('p'))) ? 0 : Input::get('p');
               <div class="alert alert-success" role="alert">
                  <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
               </div>
               <div class="row">
                  <br/>
                  <div class="col-md-12">
                     <form method="get" action="{{action('CalculadoraController@geometrica')}}">
                        <div class="form-group">
                           <label for="number">Número de experimentos
                           </label>
                           <input type="number" class="form-control" id="number" name="number" value="{{$number}}" placeholder="Digite um número">
                           <label for="p">P
                           </label>
                           <input type="float" class="form-control" id="p" name="p" value="{{$p}}" placeholder="Digite o valor de P">
                        </div>
                        <div class="button">
                           <button type="submit" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i>
                           Enviar
                           </button>
                        </div>
                     </form>
            </div>
         </div>
         <p></p>
         @if( $number && $p)
         <?php
            if($number = $_GET['number']) {
               $p = $_GET['p'];
               $q = 1-$p;
               $numerooficial = $number;
              
               $esperanca = 1/$p;
               $variancia = $q/($p*$p);
               //fazer probabilidade pro outro tipo
               $probabilidade = ($p*pow($q, $number-1))*100;
              
            
            ?>
         <div class="row">
            <div class="alert alert-info" role="alert">
               <b>Número:</b> <?=$numerooficial?><br/>
               <b>Esperança:</b> <?=$esperanca?><br/>
               <b>Variância:</b> <?=$variancia?><br/>
               <b>Probabilidade:</b>
               <?=$probabilidade?>%
               <br/>
            </div>
         </div>
      </div>
      <?php } ?>
   @endif
@stop