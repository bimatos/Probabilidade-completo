@extends('template.layout')

@section('content')
@include('clientes.relatorios')

@define $number =  (empty(Input::get('number'))) ? 0 : Input::get('number');
@define $Num =  (empty(Input::get('Num'))) ? 0 : Input::get('Num');
@define $Ndois =  (empty(Input::get('Ndois'))) ? 0 : Input::get('Ndois');
@define $x =  (empty(Input::get('x'))) ? 0 : Input::get('x');
               <div class="alert alert-success" role="alert">
                  <p style="text-align: center; font-size: 25px" > <i class="fa fa-paw" aria-hidden="true"></i> <b>Calculadora de probabilidade</b> <i class="fa fa-paw" aria-hidden="true"></i> </p>
               </div>
               <div class="row">
                  <br/>
                  <div class="col-md-12">
                     <form method="get" action="{{action('CalculadoraController@hipergeometrica')}}">
                        <div class="form-group">
                           <label for="number">Número de experimentos
                           </label>
                           <input type="number" class="form-control" id="number" name="number" value="{{$number}}" placeholder="Digite um número">
                          
                           <label for="Num">N1
                           </label>
                           <input type="float" class="form-control" id="Num" name="Num" value="{{$Num}}" placeholder="Digite o valor de Num">

                           <label for="Ndois">N2
                           </label>
                           <input type="float" class="form-control" id="Ndois" name="Ndois" value="{{$Ndois}}" placeholder="Digite o valor de Ndois">
                           <label for="x">X
                           </label>
                           <input type="float" class="form-control" id="x" name="x" value="{{$x}}" placeholder="Digite o valor de X">
                        </div>
                        <div class="button">
                           <button type="submit" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i>
                           Enviar
                           </button>
                        </div>
                     </form>
                  </div>
         </div>
         <p></p>
         @if( $number && $Num && $Ndois && $x)
         <?php
   function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }

   return $calc;
   }

            if($number = $_GET['number']) {
               $Num = $_GET['Num'];
               $Ndois = $_GET['Ndois'];
               $x = $_GET['x'];

               $N = $Num + $Ndois;
               $p = $Num/$N;
               $q = $Ndois/$N;
              
               $esperanca = $number*$p;
               $variancia = $number*$p*$q*(($N - $number)/($N-1));
            
               for ($i=0; $i <= $x; $i++) { 
               $combinacao1 = fatorial($Num) / (fatorial($i) * (fatorial($Num - $i)));

               $combinacao2 = fatorial($Ndois) / (fatorial($number-$i) * (fatorial($Ndois - ($number-$i))));

               $combinacao3 = fatorial($N) / (fatorial($number) * (fatorial($N-$number)));

               $probabilidade = $combinacao1*$combinacao2/$combinacao3;

               }
              
            
            ?>
         <div class="row">
            <div class="alert alert-info" role="alert">
               <b>Esperança:</b> <?=$esperanca?><br/>
               <b>Variância:</b> <?=$variancia?><br/>
               <b>Probabilidade:</b>
               <?=$probabilidade?>
               <br/>
            </div>
         </div>
      </div>
      <?php } ?>
   @endif
@stop