@extends('template.layout')

@section('content')
    @include('clientes.relatorios')



    <div class="row">
            @define $turma_id = Input::get('turma_id',0)

        <div class="col-md-12">
            <form action="{{action('EdefController@geral')}}">
            <div class="form-group">
                <label for="cliente_nome">Exercício</label>
                    <select class="form-control select2" id="turma_id" value="{{$turma_id}}" name="turma_id">

                        <option value="">Selecione</option>
                        <option value="1" @if($turma_id == 1) selected @endif>2016.1</option>
                    
                        
                        
                    </select>
            </div>


            
            <div class="form-group btn-cadastro">
               
                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Simular</button>
            </form>
            </div>
        @if(Input::get('turma_id') > 0)
            <div class="table-responsive">
                </div>


<div class="table-responsive">
<table class="table table-hover" width="100%">
 
    <tr>
    <th >MÉDIA GERAL DAS TURMAS</th>
  </tr>
  <tr>
    <td>{{$turmas[$turma_id]['nota_geral_curso']}}</td>   
  </tr>
  
</table>
</div>

<div class="table-responsive">
<table class="table table-hover" width="100%">
 
    <tr>
    <th >TURMA</th>
    <td> Média final</td>
    <td> Moda</td>
  </tr>
  <tr>
    <th>2014</th>
    <td>{{$turmas[$turma_id]['media_final_2014']}}</td>   
    <td>{{$turmas[$turma_id]['moda_2014']}}</td>   
  </tr>
  <tr>
    <th>2015</th>
    <td>{{$turmas[$turma_id]['media_final_2015']}}</td>   
    <td>{{$turmas[$turma_id]['moda_2015']}}</td>   
  </tr>
  <tr>
    <th>2016</th>
    <td>{{$turmas[$turma_id]['media_final_2016']}}</td>   
    <td>{{$turmas[$turma_id]['moda_2016']}}</td>   
  </tr>
  
</table>
</div>
            {{-- {{de($times[$casa_id]['jogos_casa'])}} --}}
        @endif


    </div>

@stop
