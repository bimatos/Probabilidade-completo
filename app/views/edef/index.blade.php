@extends('template.layout')

@section('content')
    @include('clientes.relatorios')


<h1>EDEF 2016.1 | Médias gerais por turma</h1>
    <div class="row">
            @define $turma_id = Input::get('turma_id',0)

        <div class="col-md-12">
            <form action="{{action('EdefController@index')}}">
            <div class="form-group">
                <label for="cliente_nome">Turma</label>
                    <select class="form-control select2" id="turma_id" value="{{$turma_id}}" name="turma_id">
                        <option value="">Selecione</option>
                        <option value="1" @if($turma_id == 1) selected @endif>2014</option>
                        <option value="2" @if($turma_id == 2) selected @endif>2015</option>
                        <option value="3" @if($turma_id == 3) selected @endif>2016</option>
                        {{-- <option value="4" @if($turma_id == 4) selected @endif>2017</option> --}}
                    </select>
            </div>


            
            <div class="form-group btn-cadastro">
                {{-- <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"> --}}
                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Simular</button>
            </form>
            </div>
        @if(Input::get('turma_id') > 0)
            <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <th>Alunos</th>
                    <th>Média | discursivas (1pt)</th>
                    <th>Quantidade | gerais</th>
                    <th>Quantidade | específicas</th>
                    <th>Média final da turma</th>
                    <th>Menor nota da turma</th>
                    <th>Maior nota da turma</th>
                </thead>
                <tbody>


                    <tr>
                        <td>
                            {{$turmas[$turma_id]['alunos']}}
                        </td>
                        <td>
                            {{$turmas[$turma_id]['media_discursiva']}}
                        </td>
                        <td>
                            {{$turmas[$turma_id]['media_conhecimento_geral']}}

                        </td>
                        <td>
                            {{$turmas[$turma_id]['media_conhecimento_especifico']}}
                        </td>
                        <td>
                            {{$turmas[$turma_id]['media_total_turma']}}
                        </td>
                        <td>
                            {{$turmas[$turma_id]['menor_nota']}}
                        </td>
                        <td>
                            {{$turmas[$turma_id]['maior_nota']}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>

<?php
function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }

   return $calc;
   }

function poisson($gol, $expectativa){
 $probabilidade = ((pow(2.719, -$expectativa)) * (pow($expectativa,$gol))) / fatorial($gol);
 return number_format($probabilidade*10, 1);
}


?>
{{-- <div class="table-responsive">
<table class="table table-hover" width="100%">
  <tr>
    <th></th>
    <th>Time de fora</th>
    <th colspan="10">{{$times[$fora_id]['nome']}}</th>
  
  </tr>
      <tr>
    <th >Time da casa</th>
    <th >gols</th>
    <th >1</th>
    <th >2</th>
    <th >3</th>
    <th >4</th>
    <th >5</th>
    <th >6</th>
    <th >7</th>
    <th >8</th>
    <th >9</th>
    <th >10</th>
  </tr>
  <tr>
    <th  rowspan="10">{{$times[$casa_id]['nome']}}</th>
    <th >1</th>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
  </tr>
  <tr>
    <th >2</th>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}% </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >3</th>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >4</th>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >5</th>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >6</th>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}% </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >7</th>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >8</th>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >9</th>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >10</th>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
 
</table>
</div> --}}
            {{-- {{de($times[$casa_id]['jogos_casa'])}} --}}
        @endif


    </div>

@stop
