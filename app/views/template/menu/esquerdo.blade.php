<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <div class="user-img-div">
                    <center>
                        <?php $imagem = "upload/perfis/".Auth::user()->foto?>
                        @if (!Auth::user()->foto)
                            <img src="{{ asset('template/assets/img/user.jpg') }}" class="img-circle">
                        @else
                            <img src="{{ asset($imagem) }}" class="img-circle">                            
                        @endif
                        <div class="perfil">
                            <?php
                                #Pega Nome e Sobrenome
                                $nome_usuario = explode(' ', trim(Auth::user()->nome));
                                $cont = count($nome_usuario)-1;
                            ?>
                            {{ $nome_usuario[0]  }}<br>
                            <?php
                            #Criptografa o ID do usuario
                            $hash = Crypt::encrypt(Auth::user()->id);
                            ?>
                        </div>
                    </center>
                </div>
            </li>
            <li {{ (Request::is('*edef*') ? 'class="active"' : '') }}>
                <a href="#"><i class="fa fa-magic "></i>EDEF <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a {{ (Request::is('*edef*') ? 'class="active-menu"' : '') }} href="{{action('EdefController@index')}}"><i class="fa fa-magic "></i>Médias por turma</a>
                    </li>
                    <li>
                        <a {{ (Request::is('*comparacao*') ? 'class="active-menu"' : '') }} href="{{action('EdefController@comparacao')}}"><i class="fa fa-magic "></i>Comparação de média de turmas</a>
                    </li>
                    <li>
                        <a {{ (Request::is('*geral*') ? 'class="active-menu"' : '') }} href="{{action('EdefController@geral')}}"><i class="fa fa-magic "></i>Média geral do curso</a>
                    </li>
                </ul>
            </li>


            <li {{ (Request::is('*simulador*') ? 'class="active"' : '') }}>
                <a href="#"><i class="fa fa-magic "></i>Problema de Monty Hall <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a {{ (Request::is('*simulador*') ? 'class="active-menu"' : '') }} href="{{action('SimuladorController@index')}}"><i class="fa fa-magic "></i>Play</a>
                    </li>
                </ul>
            </li>

          
            <li {{ (Request::is('*jogos*') ? 'class="active"' : '') }}>
                <a href="#"><i class="fa fa-futbol-o"></i>Probabilidade - Brasileirão <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a {{ (Request::is('*jogos*') ? 'class="active-menu"' : '') }} href="{{action('JogosController@index')}}"><i class="fa fa-futbol-o"></i>Play</a>
                    </li>
                </ul>
            </li>

            @if(Auth::user()->perfil_id == 1)
            <li {{ (Request::is('*calculadora*') ? 'class="active"' : '') }}>
                <a href="#"><i class="fa fa-calculator "></i>Calculadora de probabilidade <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a {{ (Request::is('*calculadora*') ? 'class="active-menu"' : '') }} href="{{action('CalculadoraController@index')}}"><i class="fa fa-calculator "></i>Play</a>
                    </li>
                </ul>
            </li>

            @endif
            <li>
                <a href="{{ url('sair') }}"><i class="fa fa-sign-out"></i>Sair</a>
            </li>
        </ul>
    </div>
</nav>