@extends('template.layout')

@section('content')
	@include('clientes.relatorios')



	<div class="row">
        	@define $casa_id = Input::get('time_casa_id',0)
        	@define $fora_id = Input::get('time_fora_id',0)

        <div class="col-md-12">
        	<form action="{{action('JogosController@index')}}">
            <div class="form-group">
                <label for="cliente_nome">Time Casa</label>
                    <select class="form-control select2" id="time_casa_nome" value="{{$casa_id}}" name="time_casa_id">
                        <option value="">Selecione</option>
                        <option value="1" @if($casa_id == 1) selected @endif>Flamengo</option>
                        <option value="2" @if($casa_id == 2) selected @endif>Corinthians</option>
                        <option value="3" @if($casa_id == 3) selected @endif>Santos</option>
                        <option value="4" @if($casa_id == 4) selected @endif>Palmeiras</option>
                        <option value="5" @if($casa_id == 5) selected @endif>São Paulo</option>
                        <option value="6" @if($casa_id == 6) selected @endif>Bahia</option>
                        <option value="7" @if($casa_id == 7) selected @endif>Grêmio</option>
                        <option value="8" @if($casa_id == 8) selected @endif>Cruzeiro</option>
                        <option value="9" @if($casa_id == 9) selected @endif>Vasco</option>
                        <option value="10" @if($casa_id == 10) selected @endif>Botafogo</option>
                        <option value="11" @if($casa_id == 11) selected @endif>Ponte Preta</option>
                        <option value="12" @if($casa_id == 12) selected @endif>Chapecoense</option>
                        <option value="13" @if($casa_id == 13) selected @endif>Atlético - PR</option>
                        <option value="14" @if($casa_id == 14) selected @endif>Sport Recife</option>
                        <option value="15" @if($casa_id == 15) selected @endif>Fluminense</option>
                        <option value="16" @if($casa_id == 16) selected @endif>Coritiba</option>
                        <option value="17" @if($casa_id == 17) selected @endif>Atlético - MG</option>
                        <option value="18" @if($casa_id == 18) selected @endif>Avaí</option>
                        <option value="19" @if($casa_id == 19) selected @endif>Atlético - GO</option>
                        <option value="20" @if($casa_id == 20) selected @endif>Vitória</option>
                    </select>
            </div>


            <div class="form-group">
                <label for="cliente_nome">Time Fora</label>
                    <select class="form-control select2" id="time_fora_nome" name="time_fora_id">
                        <option value="">Selecione</option>
                         <option value="1" @if($fora_id == 1) selected @endif>Flamengo</option>
                        <option value="2" @if($fora_id == 2) selected @endif>Corinthians</option>
                        <option value="3" @if($fora_id == 3) selected @endif>Santos</option>
                        <option value="4" @if($fora_id == 4) selected @endif>Palmeiras</option>
                        <option value="5" @if($fora_id == 5) selected @endif>São Paulo</option>
                        <option value="6" @if($fora_id == 6) selected @endif>Bahia</option>
                        <option value="7" @if($fora_id == 7) selected @endif>Grêmio</option>
                        <option value="8" @if($fora_id == 8) selected @endif>Cruzeiro</option>
                        <option value="9" @if($fora_id == 9) selected @endif>Vasco</option>
                        <option value="10" @if($fora_id == 10) selected @endif>Botafogo</option>
                        <option value="11" @if($fora_id == 11) selected @endif>Ponte Preta</option>
                        <option value="12" @if($fora_id == 12) selected @endif>Chapecoense</option>
                        <option value="13" @if($fora_id == 13) selected @endif>Atlético - PR</option>
                        <option value="14" @if($fora_id == 14) selected @endif>Sport Recife</option>
                        <option value="15" @if($fora_id == 15) selected @endif>Fluminense</option>
                        <option value="16" @if($fora_id == 16) selected @endif>Coritiba</option>
                        <option value="17" @if($fora_id == 17) selected @endif>Atlético - MG</option>
                        <option value="18" @if($fora_id == 18) selected @endif>Avaí</option>
                        <option value="19" @if($fora_id == 19) selected @endif>Atlético - GO</option>
                        <option value="20" @if($fora_id == 20) selected @endif>Vitória</option>
                    </select>
            </div>
            <div class="form-group btn-cadastro">
                {{-- <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"> --}}
                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Simular</button>
            </form>
            </div>
        @if(Input::get('time_casa_id') > 0 && Input::get('time_fora_id') > 0)
        	<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<th>Expectativa de Gols (Time Casa)</th>
					<th>Expectativa de Gols (Time Fora)</th>
					<th>Total de gols</th>
				</thead>
				<tbody>


					<tr>
						<td>
							{{$times[$casa_id]['expectativa_gols_casa']}}
						</td>
						<td>
							{{$times[$fora_id]['expectatica_gols_fora']}}

						</td>
						<td>
							{{$times[$casa_id]['expectativa_gols_casa'] + $times[$fora_id]['expectatica_gols_fora']}}
						</td>
					</tr>
				</tbody>
			</table>
		</div>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>

<?php
function fatorial($i){
   $calc = 1;
   while ($i > 1){
   $calc *= $i;
   $i--;
   }

   return $calc;
   }

function poisson($gol, $expectativa){
 $probabilidade = ((pow(2.719, -$expectativa)) * (pow($expectativa,$gol))) / fatorial($gol);
 return number_format($probabilidade*10, 1);
}


?>
<div class="table-responsive">
<table class="table table-hover" width="100%">
  <tr>
    <th></th>
    <th>Time de fora</th>
    <th colspan="10">{{$times[$fora_id]['nome']}}</th>
  
  </tr>
      <tr>
    <th >Time da casa</th>
    <th >gols</th>
    <th >1</th>
    <th >2</th>
    <th >3</th>
    <th >4</th>
    <th >5</th>
    <th >6</th>
    <th >7</th>
    <th >8</th>
    <th >9</th>
    <th >10</th>
  </tr>
  <tr>
    <th  rowspan="10">{{$times[$casa_id]['nome']}}</th>
    <th >1</th>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td class="tg-031e">{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
    <td >{{ (poisson(1, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%</td>
  </tr>
  <tr>
    <th >2</th>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}% </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(2, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >3</th>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(3, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >4</th>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td class="tg-031e">{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(4, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >5</th>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(5, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >6</th>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}% </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(6, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >7</th>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(7, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >8</th>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(8, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >9</th>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(9, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
  <tr>
    <th >10</th>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(1, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(2, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(3, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(4, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(5, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(6, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(7, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(8, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(9, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
    <td >{{ (poisson(10, $times[$casa_id]['expectativa_gols_casa'])) * (poisson(10, $times[$fora_id]['expectatica_gols_fora']
))}}%  </td>
  </tr>
 
</table>
</div>
        	{{-- {{de($times[$casa_id]['jogos_casa'])}} --}}
        @endif


    </div>

@stop
