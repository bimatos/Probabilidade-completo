@extends('template.layout')

@section('content')
@include('clientes.relatorios')
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro">
    <style>
      #vitoria{
        color: green;
      }
      #derrota{
        color: red;
      }
      #porcentagem{
        font-size: 36px;
      }
      #trocavitoria{
        color: green;
      }
      #trocaderrota{
        color: red;
      }
      #ficouvitoria{
        color: green;
      }
      #ficouderrota{
        color: red;
      }

      body {
          font-family: 'Source Sans Pro', 'Arial';
          font-size: 14px;
          background-color: #F9F9F9;
        }
      canvas { 
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        outline: none;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
      }
    </style>
  </head>

  <body>
    <h1 align="center">Monty Hall</h1>

    <table align="center">
    <tbody>
      <tr>
        <th>
          <canvas id="minhaTela" width="600" height="400"></canvas>
        </th>
        <th>
          <p>Vitória: <span id="vitoria"></span></p>
          <p>Derrota: <span id="derrota"></span></p>
          <p>Porcentagem de vitória: <br>
            <span id="porcentagem"></span> %
          </p>
          <p>Trocou de porta e ganhou: <span id="trocavitoria"></span> vezes</p>
          <p>Trocou de porta e perdeu: <span id="trocaderrota"></span> vezes</p>
          <p>Permaneceu e ganhou: <span id="ficouvitoria"></span> vezes</p>
          <p>Permaneceu e perdeu: <span id="ficouderrota"></span> vezes</p>
        </th>
      </tr>
      </tbody>
    </table>

    <p align="center"><span id="status"></span></p>

    <script>
      
      var numVitorias = 0;
      var numDerrotas = 0;
      var porcentagemValores = 0;
      var numTrocasVitorias = 0;
      var numTrocasDerrotas = 0;
      var numFicouVitorias = 0;
      var numFicouDerrotas = 0;

      
      var situacao = document.getElementById('status');
      situacao.innerHTML = 'Escolha uma porta.';

      
      var vitoria = document.getElementById('vitoria');
      vitoria.innerHTML = numVitorias;

      var derrota = document.getElementById('derrota');
      derrota.innerHTML = numDerrotas;

      var porcentagem = document.getElementById('porcentagem');
      porcentagem.innerHTML = (porcentagemValores*100).toFixed(1);

      var trocavitoria = document.getElementById('trocavitoria');
      trocavitoria.innerHTML = numTrocasVitorias;

      var trocaderrota = document.getElementById('trocaderrota');
      trocaderrota.innerHTML = numTrocasDerrotas;

      var ficouvitoria = document.getElementById('ficouvitoria');
      ficouvitoria.innerHTML = numFicouVitorias;

      var ficouderrota = document.getElementById('ficouderrota');
      ficouderrota.innerHTML = numFicouDerrotas;

      var canvas = document.getElementById('minhaTela');
      var context = canvas.getContext('2d');

      var imagemPortaAberta = new Image();
      var imagemPortaFechada = new Image();
      var imagemEscolhida = new Image();
      var imagemBode = new Image();
      var imagemCarro = new Image();

    
      imagemPortaAberta.src = '../../images/open_door.png' 
      imagemPortaFechada.src ='../../images/closed_door.png' 
      imagemEscolhida.src = '../../images/check.png' 
      imagemBode.src = '../../images/Goat.jpg' 
      imagemCarro.src = '../../images/car.png' 
      
      
      var portaCarro = Math.floor(Math.random() * 3); 
      var portaBode = []; 
      i = -1
      while(++i < 3) {
        if (i != portaCarro) {portaBode.push(i)};
      }

      imagemPortaFechada.onload = function() {
        context.drawImage(imagemPortaFechada, 0,0,200, 350);
        context.drawImage(imagemPortaFechada, 200,0,200, 350);
        context.drawImage(imagemPortaFechada, 400,0,200, 350);
      };

    
    var portas = [false,false,false]; 
    var status = 0; 
    var check = 0; // Primeira porta escolhida
    var portaAberta = 0; // Porta escolhida após permanecer ou ficar

    canvas.addEventListener('click', function(event) {
      var rect = canvas.getBoundingClientRect();
      var x = event.pageX - rect.left;
      var y = event.pageY - rect.top;

      /* Status dos jogos (portaCarro):
        -1: Jogo acabou
         0: Jogador escolhe uma porta
         1: Jogador escolhe ficar ou trocar
      */
      if(status == -1){ // Restart
          portas = [false,false,false];
          check = 0;
          portaAberta = 0;
          portaCarro = Math.floor(Math.random() * 3);
          portaBode = [];
          i = -1
          while(++i < 3) {
            if (i != portaCarro) {portaBode.push(i)};
          }
          context.clearRect(0,0,canvas.width,canvas.height)
          context.drawImage(imagemCarro, portaCarro*200+50,200,120, 80);
          portaBode.forEach(function(d){
            context.drawImage(imagemBode, d*200 + 75,180,101, 100);
          });
          context.drawImage(imagemPortaFechada, 0,0,200, 350);
          context.drawImage(imagemPortaFechada, 200,0,200, 350);
          context.drawImage(imagemPortaFechada, 400,0,200, 350);
          situacao.innerHTML = 'Quer tentar de novo? Escolha uma porta.';
          ++status;
      } else {
        if (status == 0){ // Porta selecionada
          check = 0;
          if (x < 200 && x > 0) {check = 0;}
          if (x < 400 && x > 200) {check = 1;}
          if (x < 600 && x > 400) {check = 2;}

          portaAberta = Math.floor(Math.random() * 3);
          while(portaAberta == check || portaAberta == portaCarro){
            portaAberta = Math.floor(Math.random() * 3);
          }
          portas[portaAberta] = true;
          context.clearRect(0,0,canvas.width,canvas.height);
          context.drawImage(imagemCarro, portaCarro*200+50,200,120, 80);
          portaBode.forEach(function(d){
            context.drawImage(imagemBode, d*200 + 75,180,101, 100);
          });
          i = -1;
          while(++i < 3){
            if(!portas[i]){
              context.drawImage(imagemPortaFechada, i*200,0,200, 350);
            }
            else{
              context.drawImage(imagemPortaAberta, i*200-2,0,212, 370);
            }
          }
          context.drawImage(imagemEscolhida, check*200+50,50,50,50);
          situacao.innerHTML = 'Deseja permanecer ou trocar de porta?'
          ++status;
        }
        else{
          if (status > 0){ //Abrir porta final
            open = 0;
            if (x < 200 && x > 0) {open = 0;}
            if (x < 400 && x > 200) {open = 1;}
            if (x < 600 && x > 400) {open = 2;}

            if(open == portaAberta){
              return;
            }
            portas[open] = true;
            context.clearRect(0,0,canvas.width,canvas.height)
            context.drawImage(imagemCarro, portaCarro*200+50,200,120, 80);
            portaBode.forEach(function(d){
              context.drawImage(imagemBode, d*200 + 75,180,101, 100);
            });
            i = -1;
            while(++i < 3){
              if(!portas[i]){
                 context.drawImage(imagemPortaFechada, i*200,0,200, 350);
              }
              else{
                 context.drawImage(imagemPortaAberta, i*200-2,0,212, 370);
              }
            }
            context.drawImage(imagemEscolhida, check*200+50,50,50,50);

            if(open == portaCarro){
              if(open != check){
                ++numTrocasVitorias;
              }
              if(open == check) {
                ++numFicouVitorias;
              }

              ++numVitorias;
              situacao.innerHTML = 'Parabéns! Você venceu';
            }
            else {
              if(open != check){
                ++numTrocasDerrotas;
              }
              if(open == check) {
                ++numFicouDerrotas;
              }
              ++numDerrotas;
              situacao.innerHTML = 'Ops! Não foi dessa vez :)';
            }
            porcentagemValores = 1.0*numVitorias/(numVitorias+numDerrotas);

            // Uptate nos resultados
            trocavitoria.innerHTML = numTrocasVitorias;
            trocaderrota.innerHTML = numTrocasDerrotas;
            ficouvitoria.innerHTML = numFicouVitorias;
            ficouderrota.innerHTML = numFicouDerrotas;
            vitoria.innerHTML = numVitorias;
            derrota.innerHTML = numDerrotas;
            porcentagem.innerHTML = (porcentagemValores*100).toFixed(1);

            status = -1;
          };
        }
      }
    }, false);
    </script>
@stop