<?php

class UsuarioTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        #Apaga e povoa a tabela de usuarios
        DB::table('usuarios')->delete();

        Usuario::create(array(
                'email' => 'administrador@gmail.com',
                'senha' => Hash::make('senha'),
                'nome'  => 'Lorena/Bianca',
                'perfil_id'  => 1
        ));

        
        
	}

}