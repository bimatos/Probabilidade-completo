<?php

class JogosController extends \HelpersController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$times = array(
	    1 => [
            'nome'         => 'Flamengo',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 10,
            'empate_casa'  => 6,
            'derrota_casa'  => 3,
            'gols_casa'  => 32,
            'gols_sofridos_casa'  => 13,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 5,
            'derrota_fora'  => 9,
            'gols_fora'  => 17,
            'gols_sofridos_fora'  => 25,
            'forca_ataque_casa' => 1.22,
            'forca_defesa_casa'	=> 0.66,
            'forca_ataque_fora'	=> 0.86,
            'forca_defesa_fora'	=> 0.93,
            'media_gols_casa'	=> 1.68,
            'media_gols_fora'	=> 0.89,
            'expectativa_gols_casa'	=> 1.90,
            'expectatica_gols_fora'	=>0.50
        ],
        2 => [
            'nome'         => 'Corinthians',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 12,
            'empate_casa'  => 5,
            'derrota_casa'  => 2,
            'gols_casa'  => 32,
            'gols_sofridos_casa'  => 15,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 9,
            'empate_fora'  => 4,
            'derrota_fora'  => 6,
            'gols_fora'  => 18,
            'gols_sofridos_fora'  => 15,
            'forca_ataque_casa' => 1.22,
            'forca_defesa_casa'	=> 0.76,
            'forca_ataque_fora'	=> 0.91,
            'forca_defesa_fora'	=> 0.56,
            'media_gols_casa'	=> 1.68,
            'media_gols_fora'	=> 0.95,
            'expectativa_gols_casa'	=> 1.14,
            'expectatica_gols_fora'	=> 0.65
        ],

         3 => [
            'nome'         => 'Santos',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 12,
            'empate_casa'  => 4,
            'derrota_casa'  => 3,
            'gols_casa'  => 25,
            'gols_sofridos_casa'  => 12,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 8,
            'derrota_fora'  => 6,
            'gols_fora'  => 17,
            'gols_sofridos_fora'  => 20,
            'forca_ataque_casa' => 0.95,
            'forca_defesa_casa'	=> 0.60,

            'forca_ataque_fora'	=> 0.86,
            'forca_defesa_fora'	=> 0.75,
            'media_gols_casa'	=> 1.32,
            'media_gols_fora'	=> 0.89,
            'expectativa_gols_casa'	=> 0.94,
            'expectatica_gols_fora'	=> 0.45
        ],
        4 => [
            'nome'         => 'Palmeiras',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 12,
            'empate_casa'  => 3,
            'derrota_casa'  => 4,
            'gols_casa'  => 35,
            'gols_sofridos_casa'  => 16,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 7,
            'empate_fora'  => 3,
            'derrota_fora'  => 9,
            'gols_fora'  => 26,
            'gols_sofridos_fora'  => 29,
            'forca_ataque_casa' => 1.33,
            'forca_defesa_casa'	=> 0.81,
            'forca_ataque_fora'	=> 1.31,
            'forca_defesa_fora'	=> 1.08,
            'media_gols_casa'	=> 1.84,
            'media_gols_fora'	=> 1.37,
            'expectativa_gols_casa'	=> 2.64,
            'expectatica_gols_fora'	=> 1.45
        ],
        5 => [
            'nome'         => 'São Paulo',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 9,
            'empate_casa'  => 8,
            'derrota_casa'  => 2,
            'gols_casa'  => 29,
            'gols_sofridos_casa'  => 18,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 4,
            'empate_fora'  => 3,
            'derrota_fora'  => 12,
            'gols_fora'  => 19,
            'gols_sofridos_fora'  => 31,
            'forca_ataque_casa' => 1.10,
            'forca_defesa_casa'	=> 0.91,
            'forca_ataque_fora'	=> 0.96,
            'forca_defesa_fora'	=> 1.16,
            'media_gols_casa'	=> 1.53,
            'media_gols_fora'	=> 1.00,
            'expectativa_gols_casa'	=> 1.95,
            'expectatica_gols_fora'	=> 0.87
        ],
        6 => [
            'nome'         => 'Bahia',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 10,
            'empate_casa'  => 4,
            'derrota_casa'  => 5,
            'gols_casa'  => 34,
            'gols_sofridos_casa'  => 21,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 3,
            'empate_fora'  => 7,
            'derrota_fora'  => 9,
            'gols_fora'  => 16,
            'gols_sofridos_fora'  => 27,
            'forca_ataque_casa' => 1.29,
            'forca_defesa_casa'	=> 1.06,
            'forca_ataque_fora'	=> 0.81,
            'forca_defesa_fora'	=> 1.01,
            'media_gols_casa'	=> 1.79,
            'media_gols_fora'	=> 0.84,
            'expectativa_gols_casa'	=> 2.33,
            'expectatica_gols_fora'	=> 0.72
        ],
        7 => [
            'nome'         => 'Grêmio',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 10,
            'empate_casa'  => 4,
            'derrota_casa'  => 5,
            'gols_casa'  => 26,
            'gols_sofridos_casa'  => 13,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 8,
            'empate_fora'  => 4,
            'derrota_fora'  => 7,
            'gols_fora'  => 29,
            'gols_sofridos_fora'  => 23,
            'forca_ataque_casa' => 0.99,
            'forca_defesa_casa'	=> 0.66,
            'forca_ataque_fora'	=> 1.46,
            'forca_defesa_fora'	=> 0.86,
            'media_gols_casa'	=> 1.37,
            'media_gols_fora'	=> 1.53,
            'expectativa_gols_casa'	=> 1.16,
            'expectatica_gols_fora'	=> 1.47
        ],
        8 => [
            'nome'         => 'Cruzeiro',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 9,
            'empate_casa'  => 7,
            'derrota_casa'  => 3,
            'gols_casa'  => 26,
            'gols_sofridos_casa'  => 17,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 6,
            'empate_fora'  => 5,
            'derrota_fora'  => 8,
            'gols_fora'  => 21,
            'gols_sofridos_fora'  => 22,
            'forca_ataque_casa' => 0.99,
            'forca_defesa_casa'	=> 0.86,
            'forca_ataque_fora'	=> 1.06,
            'forca_defesa_fora'	=> 0.82,
            'media_gols_casa'	=> 1.37,
            'media_gols_fora'	=> 1.11,
            'expectativa_gols_casa'	=> 1.11,
            'expectatica_gols_fora'	=> 1.01
        ],
        9 => [
            'nome'         => 'Vasco',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 8,
            'empate_casa'  => 7,
            'derrota_casa'  => 4,
            'gols_casa'  => 21,
            'gols_sofridos_casa'  => 21,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 7,
            'empate_fora'  => 4,
            'derrota_fora'  => 8,
            'gols_fora'  => 19,
            'gols_sofridos_fora'  => 26,
            'forca_ataque_casa' => 0.80,
            'forca_defesa_casa'	=> 1.06,
            'forca_ataque_fora'	=> 0.96,
            'forca_defesa_fora'	=> 0.97,
            'media_gols_casa'	=> 1.11,
            'media_gols_fora'	=> 1.00,
            'expectativa_gols_casa'	=> 0.86,
            'expectatica_gols_fora'	=> 1.01
        ],
        10 => [
            'nome'         => 'Botafogo',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 9,
            'empate_casa'  => 3,
            'derrota_casa'  => 7,
            'gols_casa'  => 30,
            'gols_sofridos_casa'  => 25,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 8,
            'derrota_fora'  => 6,
            'gols_fora'  => 15,
            'gols_sofridos_fora'  => 17,
            'forca_ataque_casa' => 1.14,
            'forca_defesa_casa'	=> 1.26,
            'forca_ataque_fora'	=> 0.76,
            'forca_defesa_fora'	=> 1.01,
            'media_gols_casa'	=> 1.58,
            'media_gols_fora'	=> 0.79,
            'expectativa_gols_casa'	=> 1.81,
            'expectatica_gols_fora'	=> 0.75
        ],
        11 => [
            'nome'         => 'Ponte Preta',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 9,
            'empate_casa'  => 3,
            'derrota_casa'  => 7,
            'gols_casa'  => 26,
            'gols_sofridos_casa'  => 21,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 1,
            'empate_fora'  => 6,
            'derrota_fora'  => 12,
            'gols_fora'  => 11,
            'gols_sofridos_fora'  => 31,
            'forca_ataque_casa' => 0.99,
            'forca_defesa_casa'	=> 1.06,
            'forca_ataque_fora'	=> 0.55,
            'forca_defesa_fora'	=> 1.16,
            'media_gols_casa'	=> 1.37,
            'media_gols_fora'	=> 0.58,
            'expectativa_gols_casa'	=> 1.57,
            'expectatica_gols_fora'	=> 0.33
        ],
        12 => [
            'nome'         => 'Chapecoense',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 9,
            'empate_casa'  => 3,
            'derrota_casa'  => 7,
            'gols_casa'  => 24,
            'gols_sofridos_casa'  => 21,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 6,
            'empate_fora'  => 6,
            'derrota_fora'  => 7,
            'gols_fora'  => 23,
            'gols_sofridos_fora'  => 28,
            'forca_ataque_casa' => 0.91,
            'forca_defesa_casa'	=> 1.06,
            'forca_ataque_fora'	=> 1.16,
            'forca_defesa_fora'	=> 1.05,
            'media_gols_casa'	=> 1.26,
            'media_gols_fora'	=> 1.21,
            'expectativa_gols_casa'	=> 1.20,
            'expectatica_gols_fora'	=> 1.48
        ],
        13 => [
            'nome'         => 'Atlético - PR',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 8,
            'empate_casa'  => 5,
            'derrota_casa'  => 6,
            'gols_casa'  => 29,
            'gols_sofridos_casa'  => 20,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 6,
            'empate_fora'  => 4,
            'derrota_fora'  => 9,
            'gols_fora'  => 16,
            'gols_sofridos_fora'  => 23,
            'forca_ataque_casa' => 1.10,
            'forca_defesa_casa'	=> 1.01,
            'forca_ataque_fora'	=> 0.81,
            'forca_defesa_fora'	=> 0.86,
            'media_gols_casa'	=> 1.53,
            'media_gols_fora'	=> 0.84,
            'expectativa_gols_casa'	=> 1.44,
            'expectatica_gols_fora'	=> 0.68
        ],
        14 => [
            'nome'         => 'Sport Recife',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 7,
            'empate_casa'  => 7,
            'derrota_casa'  => 5,
            'gols_casa'  => 27,
            'gols_sofridos_casa'  => 21,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 2,
            'derrota_fora'  => 12,
            'gols_fora'  => 19,
            'gols_sofridos_fora'  => 37,
            'forca_ataque_casa' => 1.03,
            'forca_defesa_casa'	=> 1.06,
            'forca_ataque_fora'	=> 0.96,
            'forca_defesa_fora'	=> 1.38,
            'media_gols_casa'	=> 1.42,
            'media_gols_fora'	=> 1.00,
            'expectativa_gols_casa'	=> 2.01,
            'expectatica_gols_fora'	=> 1.01
        ],
        15 => [
            'nome'         => 'Fluminense',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 7,
            'empate_casa'  => 6,
            'derrota_casa'  => 6,
            'gols_casa'  => 27,
            'gols_sofridos_casa'  => 24,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 4,
            'empate_fora'  => 8,
            'derrota_fora'  => 7,
            'gols_fora'  => 23,
            'gols_sofridos_fora'  => 29,
            'forca_ataque_casa' => 1.03,
            'forca_defesa_casa'	=> 1.21,
            'forca_ataque_fora'	=> 1.16,
            'forca_defesa_fora'	=> 1.08,
            'media_gols_casa'	=> 1.42,
            'media_gols_fora'	=> 1.21,
            'expectativa_gols_casa'	=> 1.57,
            'expectatica_gols_fora'	=> 1.69
        ],
        16 => [
            'nome'         => 'Coritiba',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 7,
            'empate_casa'  => 5,
            'derrota_casa'  => 7,
            'gols_casa'  => 21,
            'gols_sofridos_casa'  => 18,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 4,
            'empate_fora'  => 5,
            'derrota_fora'  => 10,
            'gols_fora'  => 21,
            'gols_sofridos_fora'  => 33,
            'forca_ataque_casa' => 0.80,
            'forca_defesa_casa'	=> 0.91,
            'forca_ataque_fora'	=> 1.06,
            'forca_defesa_fora'	=> 1.23,
            'media_gols_casa'	=> 1.11,
            'media_gols_fora'	=> 1.11,
            'expectativa_gols_casa'	=> 1.09,
            'expectatica_gols_fora'	=> 	1.07
        ],
        17 => [
            'nome'         => 'Atlético - MG',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 7,
            'empate_casa'  => 4,
            'derrota_casa'  => 8,
            'gols_casa'  => 27,
            'gols_sofridos_casa'  => 27,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 7,
            'empate_fora'  => 8,
            'derrota_fora'  => 4,
            'gols_fora'  => 25,
            'gols_sofridos_fora'  => 22,
            'forca_ataque_casa' => 1.03,
            'forca_defesa_casa'	=> 1.36,
            'forca_ataque_fora'	=> 1.26,
            'forca_defesa_fora'	=> 0.82,
            'media_gols_casa'	=> 1.42,
            'media_gols_fora'	=> 1.32,
            'expectativa_gols_casa'	=> 1.19,
            'expectatica_gols_fora'	=> 2.26
        ],
        18 => [
            'nome'         => 'Avaí',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 5,
            'empate_casa'  => 9,
            'derrota_casa'  => 5,
            'gols_casa'  => 15,
            'gols_sofridos_casa'  => 20,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 4,
            'derrota_fora'  => 10,
            'gols_fora'  => 14,
            'gols_sofridos_fora'  => 28,
            'forca_ataque_casa' => 0.57,
            'forca_defesa_casa'	=> 1.01,
            'forca_ataque_fora'	=> 0.71,
            'forca_defesa_fora'	=> 1.05,
            'media_gols_casa'	=> 0.79,
            'media_gols_fora'	=> 0.74,
            'expectativa_gols_casa'	=> 0.47,
            'expectatica_gols_fora'	=> 0.53
        ],
        19 => [
            'nome'         => 'Atlético - GO',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 4,
            'empate_casa'  => 5,
            'derrota_casa'  => 10,
            'gols_casa'  => 18,
            'gols_sofridos_casa'  => 23,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 5,
            'empate_fora'  => 4,
            'derrota_fora'  => 10,
            'gols_fora'  => 20,
            'gols_sofridos_fora'  => 33,
            'forca_ataque_casa' => 0.68,
            'forca_defesa_casa'	=> 1.16,
            'forca_ataque_fora'	=> 1.01,
            'forca_defesa_fora'	=> 1.23,
            'media_gols_casa'	=> 0.95,
            'media_gols_fora'	=> 1.05,
            'expectativa_gols_casa'	=> 0.79,
            'expectatica_gols_fora'	=> 1.23
        ],
        20 => [
            'nome'         => 'Vitória',
            'jogos_casa'  => 19,
            'vitorias_casa'  => 3,
            'empate_casa'  => 5,
            'derrota_casa'  => 11,
            'gols_casa'  => 22,
            'gols_sofridos_casa'  => 31,
            'jogos_fora'  => 19,
            'vitorias_fora'  => 8,
            'empate_fora'  => 5,
            'derrota_fora'  => 6,
            'gols_fora'  => 28,
            'gols_sofridos_fora'  => 33,
            'forca_ataque_casa' => 0.84,
            'forca_defesa_casa'	=> 1.56,
            'forca_ataque_fora'	=> 1.41,
            'forca_defesa_fora'	=> 1.01,
            'media_gols_casa'	=> 1.16,
            'media_gols_fora'	=> 1.47,
            'expectativa_gols_casa'	=> 0.98,
            'expectatica_gols_fora'	=> 3.23
        ],
);
		// de($times);


		return View::make('jogos.index')->with(['times' => $times]);
	}

	/**
	 * Show the form for creating a new resource.
	 *	 * @return Response
	 */
	public function create()
	{
		$telefoneTipo = TelefoneTipo::get();

		return View::make('clientes.create', compact('telefoneTipo'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$matricula = substr(hexdec(uniqid()), -8);

		$inputCliente = Input::get('cliente');
		$inputEnderecoCorresp = Input::get('endereco');
		$inputTelefone = Input::get('telefone');

		$inputs = array_merge_recursive($inputCliente, $inputEnderecoCorresp, $inputTelefone);


		$validatorCliente = Validator::make($inputCliente, Cliente::$rules);
		$validatorEnderecoCorresp = Validator::make($inputEnderecoCorresp, EnderecoCorrespondencia::$rules);
		$validatorTelefone = Validator::make($inputTelefone, Telefone::$rules);


		if ($validatorCliente->fails() || $validatorEnderecoCorresp->fails() || $validatorTelefone->fails())
		{
			# Mescla os arrays de erros
			$errors = $validatorCliente->errors();
			$errors->merge($validatorEnderecoCorresp->errors());
			$errors->merge($validatorTelefone->errors());

			return Redirect::action('ClientesController@create')->withErrors($errors)->withInput($inputs);
		}

		//Manipula a data para ser inserida no banco na forma correta
		$inputCliente['data_emissao']    = $this->handleDate($inputCliente['data_emissao']);
		$inputCliente['data_nascimento'] = $this->handleDate($inputCliente['data_nascimento']);
		$inputCliente['associado']       = $this->handleDate($inputCliente['associado']);
		$inputCliente['matricula']       = $matricula;

		$cliente = new Cliente(array_filter($inputCliente));
		$endereco = new EnderecoCorrespondencia($inputEnderecoCorresp);
		$telefone = new Telefone($inputTelefone);

		$cliente->save();
		$cliente->enderecoCorrespondencia()->save($endereco);
		
		$cliente->telefone()->save($telefone);

	    return Redirect::action('ClientesController@index')->with('mensagem', 'Cliente cadastrado com sucesso.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cliente = Cliente::with('enderecoCorrespondencia', 'telefone')->find($id);
		$telefoneTipo = TelefoneTipo::get();

		// return Response::json($cliente->dependentes);
		return View::make('clientes.edit', compact('cliente', 'telefoneTipo'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cliente = Cliente::find($id);

		$inputCliente = Input::get('cliente');
		$inputEnderecoCorresp = Input::get('endereco');
		$inputTelefone = Input::get('telefone');

		$inputs = array_merge_recursive($inputCliente, $inputEnderecoCorresp, $inputTelefone);

		$validatorCliente = Validator::make($inputCliente, $this->handleValidation(Cliente::$rules, $id));
		$validatorEnderecoCorresp = Validator::make($inputEnderecoCorresp, EnderecoCorrespondencia::$rules);
		$validatorTelefone = Validator::make($inputTelefone, Telefone::$rules);

		if ($validatorCliente->fails() || $validatorEnderecoCorresp->fails() || $validatorTelefone->fails())
		{
			# Mescla os arrays de erros
			$errors = $validatorCliente->errors();
			$errors->merge($validatorEnderecoCorresp->errors());
			$errors->merge($validatorTelefone->errors());

			return Redirect::action('ClientesController@edit', $id)->withErrors($errors)->withInput($inputs);
		}

		$inputCliente['data_emissao'] = $this->handleDate($inputCliente['data_emissao']);
		$inputCliente['data_nascimento'] = $this->handleDate($inputCliente['data_nascimento']);
		// Caso alguém altere o valor do input hidden oportunidade_id, não afetará os dados da tabela, visto que
		// este valor nunca mudará a partir do momento em que o cliente é cadastrado.
		$inputCliente['oportunidade_id'] = $cliente->oportunidade_id;

		$cliente->update(array_filter($inputCliente));
		$cliente->enderecoCorrespondencia()->update($inputEnderecoCorresp);
		$cliente->telefone()->update($inputTelefone);

	    return Redirect::action('ClientesController@index')->with('mensagem', 'Cliente atualizado com sucesso.');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$dependentes = Cliente::has('dependentes')->find($id);
		$cobrancas = Cliente::has('cobrancas')->find($id);

		// Exclui o cliente se ele não houver relacionamento com dependentes e cobranças
		if (empty($dependentes) && empty($cobrancas))
		{
			$cliente = Cliente::find($id);
			$cliente->ativo = 0;
			$cliente->save();
			return Redirect::action('ClientesController@index')->with('mensagem', 'Cliente excluído com sucesso.');
		}

		return Redirect::back()->with('mensagem', 'Não foi possível excluir o cliente. Ele tem vínculo com dependentes ou cobranças.');

	}

	/**
	 *
	 * Esses dois métodos são para a inclusão de dependente diretamente da view index do cliente
	 */
	public function createDependente($id)
	{
		$cliente = Cliente::find($id);
		$tipoDependente = DependenteTipo::all();

		return View::make('dependentes.create', compact('cliente', 'tipoDependente'))->with('clienteController', true);
	}

	public function storeDependente()
	{
		$id = Input::get('cliente_id');

		$input = Input::all();

		$validator = Validator::make($input, Dependente::$rules);

		if ($validator->fails())
		{
			return Redirect::action('ClientesController@createDependente', $id)->withErrors($validator)->withInput();
		}

		$dependente = new Dependente(array_filter($input));
		$dependente->save();

	    return Redirect::action('DependentesController@index')->with('mensagem', 'Dependente cadastrado com sucesso.');
	}

}
