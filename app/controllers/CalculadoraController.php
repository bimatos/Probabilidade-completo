<?php

class CalculadoraController extends BaseController {


    public function index()
    {
        return View::make('calculadora.index');
    }

    public function binomial()
    {
        return View::make('calculadora.binomial');
    }

    public function geometrica()
    {
        return View::make('calculadora.geometrica');
    }

    public function binomialnegativa()
    {
        return View::make('calculadora.binomialnegativa');
    }

    public function hipergeometrica()
    {
        return View::make('calculadora.hipergeometrica');
    }


    public function poisson()
    {
        return View::make('calculadora.poisson');
    }


    
}