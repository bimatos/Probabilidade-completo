<?php

class EdefController extends \HelpersController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$turmas = array(
	    1 => [
            'nome'         => 'Turma 2014',
            'alunos'         => 21,
            'media_discursiva'  => 0.7,
            'media_conhecimento_geral'  => 5.1,
            'media_conhecimento_especifico'  => 18.3,
            'media_total_turma'  => 6.4,
            'menor_nota' => 4.09,
            'maior_nota' => 8.11,
            ],
        2 => [
            'nome'         => 'Turma 2015',
            'alunos'         => 25,
            'media_discursiva'  => 0.65,
            'media_conhecimento_geral'  => 5.48,
            'media_conhecimento_especifico'  => 16.4,
            'media_total_turma'  => 5.77,
            'menor_nota' => 3.70,
            'maior_nota' => 8.58,
            ],
        3 => [
            'nome'         => 'Turma 2016',
            'alunos'         => 31,
            'media_discursiva'  => 0.51,
            'media_conhecimento_geral'  => 5.22,
            'media_conhecimento_especifico'  => 15.77,
            'media_total_turma'  => 5.54,
            'menor_nota' => 4.03,
            'maior_nota' => 7.13,
            ],
        
);
		// de($times);


		return View::make('edef.index')->with(['turmas' => $turmas]);
	}

	public function comparacao()
	{
		$turmas = array(
	  1 => [
            'nome'         => 'Ano 2016.1',
            'media_final_2014'  => 6.4,
            'moda_2014'  => 6.19,
            'media_final_2015'  => 5.77,
            'moda_2015'  => 6.14,
            'media_final_2016'  => 5.54,
            'moda_2016'  => 6.08,
            ],
        2 => [
            'nome'         => 'Ano 2016.2',
            'media_final_2014'  => 5.52,
            'moda_2014'  => '-',
            'media_final_2015'  => 5.61,
            'moda_2015'  => 0,
            'media_final_2016'  => 5.51,
            'moda_2016'  => 0,
            ],
        
        
);
		// de($times);


		return View::make('edef.comparacao')->with(['turmas' => $turmas]);
	}

public function geral()
	{
		$turmas = array(
        1 => [
            'nome'         => 'Ano 2016.1',
            'media_final_2014'  => 6.4,
            'moda_2014'  => 6.19,
            'media_final_2015'  => 5.77,
            'moda_2015'  => 6.14,
            'media_final_2016'  => 5.54,
            'moda_2016'  => 6.08,
            'nota_geral_curso' => 5.90,
            'mediana_geral_curso' => 5.77
            ],

        
);
		// de($times);


		return View::make('edef.geral')->with(['turmas' => $turmas]);
	}


	
}
