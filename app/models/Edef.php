<?php

class Edef extends Eloquent {

	/**
	 * Tabela usada pela model no banco
	 *
	 * @var string
	 */
	protected $table = 'edef';

	/*
	 *
	 * variável fillable é usada para atribuição em massa
	 * http://laravel.com/docs/4.2/eloquent#mass-assignment
	*/
	protected $fillable = [
		'turma_id',
		'alunos',
		'media_total_turma',
		'media_discursiva',
		'media_conhecimento_especifico'
		'moda_2014'
		'moda_2015'
		'moda_2016'
		'media_final_2014'
		'media_final_2015'
		'media_final_2016'
		];



}