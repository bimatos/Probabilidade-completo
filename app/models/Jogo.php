<?php

class Jogos extends Eloquent {

	/**
	 * Tabela usada pela model no banco
	 *
	 * @var string
	 */
	protected $table = 'jogos';

	/*
	 *
	 * variável fillable é usada para atribuição em massa
	 * http://laravel.com/docs/4.2/eloquent#mass-assignment
	*/
	protected $fillable = [
		'time_casa_id',
		'time_fora_id',
		'gols_casa',
		'gols_fora',
		'vitoria_casa',
		'empate',
		'vitoria_fora'
		];



}